var express = require("express");
var app = express();
var PORT = process.env.PORT || 3000;

app.all("*", function(req, res) {
    var responseString = "Hello!";
    console.log(responseString);
    res.status(200).send(responseString);
    return res;
});

app.listen(PORT, function() {
    console.log("Server up and running on port " + PORT);
});
